Feature: Hello World
  Show one BDD test running with a Hello World skill
  [this will verify the tools are installed on local machines]

  Scenario: I have installed cucumber and BDD works
    Given Alexa launches the HelloAlexa skill
    When the HelloAlexa lambda function runs
    Then it returns a HelloWorld voice response