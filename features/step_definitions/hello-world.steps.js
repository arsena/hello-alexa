'use strict';

var {defineSupportCode} = require('cucumber');
var assert = require('assert');
var MyLambdaFunction = require('../../src/index.js');

defineSupportCode(function({Given, Then, When}) {

    var mocks = {
        event: {
            "session": {
                "sessionId": "SessionId.f9e6dcbb-b7da-4b47-905c.etc.etc",
                "application": {
                    "applicationId": "amzn1.hello-alexa.app.1234"
                },
                "attributes": {},
                "user": {
                    "userId": "amzn1.ask.account.VO3PVTGF563MOPBY.etc.etc"
                },
                "new": true
            },
            "request": {
                "type": "LaunchRequest",
                "requestId": "request5678",
                "locale": "en-US"
            },
            "version": "1.0"
        },
        response: null
    };

    Given(/^Alexa launches the HelloAlexa skill$/, function () {
    });

    When(/^the HelloAlexa lambda function runs$/, function (callback) {
        var context = {
            'succeed': function (data) {
                // console.log(JSON.stringify(data, null, '\t'));
                mocks.response = data;
                callback();
            },
            'fail': function (err) {
                // console.log('context.fail occurred');
                // console.log(JSON.stringify(err, null, '\t'));
                callback(err);
            }
        };
        MyLambdaFunction['handler'] (mocks.event, context);
    });

    Then(/^it returns a HelloWorld voice response$/, function() {
        assert.ok(mocks.response);
    });

});